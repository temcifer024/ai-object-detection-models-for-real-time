# Import necessary libraries
import numpy
import torch
import cv2
import time
import random
import subprocess
import psutil
from ultralytics import YOLOv10
import os
import pandas as pd
import matplotlib.pyplot as plt
import urllib.request

# Create a directory for the weights in the current working directory
weights_dir = os.path.join(os.getcwd(), "YOLOv10 Weights")
os.makedirs(weights_dir, exist_ok=True)

# URLs of the weight files
urls = [
    "https://github.com/jameslahm/yolov10/releases/download/v1.0/yolov10n.pt",
    "https://github.com/jameslahm/yolov10/releases/download/v1.0/yolov10s.pt",
    "https://github.com/jameslahm/yolov10/releases/download/v1.0/yolov10m.pt",
    "https://github.com/jameslahm/yolov10/releases/download/v1.0/yolov10b.pt",
    "https://github.com/jameslahm/yolov10/releases/download/v1.0/yolov10x.pt",
    "https://github.com/jameslahm/yolov10/releases/download/v1.0/yolov10l.pt"
]

# Download each file if it doesn't already exist
for url in urls:
    file_name = os.path.join(weights_dir, os.path.basename(url))
    if not os.path.exists(file_name):
        urllib.request.urlretrieve(url, file_name)
        print(f"Downloaded {file_name}")
    else:
        print(f"{file_name} already exists. Skipping download.")


# Set device to GPU if available, else CPU
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model_path = 'YOLOv10 Weights/yolov10x.pt'

model = YOLOv10(model_path)
model.to(device)

class_names = ['person', 'bicycle', 'car', 'motorbike', 'aeroplane', 'bus', 'train', 'truck', 'boat']
label_map = model.names
for i, class_name in enumerate(class_names):
    label_map[i] = class_name
COLORS = [[random.randint(0, 255) for _ in range(3)] for _ in label_map]

# Function for YOLOv10 detection on an image
def yolov10_detection(model, image):
    results = model.predict(image, imgsz=640, conf=0.2, verbose=False)
    result = results[0].cpu()
    box = result.boxes.xyxy.numpy()
    conf = result.boxes.conf.numpy()
    class_id = result.boxes.cls.numpy().astype(int)
    return class_id, conf, box

# Function to determine text color based on box color brightness
def get_text_color(box_color):
    text_color = (255, 0, 0)
    brightness = box_color[2] * 0.299 + box_color[1] * 0.587 + box_color[0] * 0.114
    if brightness > 180:
        text_color = (0, 0, 0)
    return text_color

# Function to draw FPS on the image
def draw_fps(avg_fps, combined_img):
    avg_fps_str = float("{:.2f}".format(avg_fps))
    cv2.putText(combined_img, str(avg_fps_str), (20, 90), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), thickness=4)
    return combined_img

# Function to extract file name from the source path
def get_name(source_path):
    name_idx = 0
    file_pos = (source_path).rfind('\\')
    if file_pos == -1:
        file_pos = (source_path).rfind('/')
        if file_pos == -1:
            file_pos = 0
    name_idx = file_pos + 1
    name = source_path[name_idx:]
    return name

# Function to generate the save path for annotated video
def get_save_path(file_name, folder_name):
    path = "Annoted_Video_Yolov10x"
    save_path = os.path.join(path, folder_name)
    exists = os.path.exists(save_path)
    if not exists:
        os.makedirs(save_path)
    save_path = os.path.join(save_path, file_name)
    return save_path

# Function to draw bounding boxes and labels on the image
def draw_box(img, detection_output, class_list, colors):
    out_image = img
    for run_output in detection_output:
        label, con, box = run_output
        box_color = colors[int(label.item())]
        text_color = get_text_color(box_color)
        label = class_list[int(label.item())]
        first_half_box = (int(box[0].item()), int(box[1].item()))
        second_half_box = (int(box[2].item()), int(box[3].item()))
        cv2.rectangle(out_image, first_half_box, second_half_box, box_color, 2)
        text_print = '{label} {con:.2f}'.format(label=label, con=con.item())
        text_location = (int(box[0]), int(box[1] - 10))
        labelSize, baseLine = cv2.getTextSize(text_print, cv2.FONT_HERSHEY_SIMPLEX, 1, 1)
        cv2.rectangle(out_image, (int(box[0]), int(box[1] - labelSize[1] - 10)), (int(box[0]) + labelSize[0], int(box[1] + baseLine - 10)), box_color, cv2.FILLED)
        cv2.putText(out_image, text_print, text_location, cv2.FONT_HERSHEY_SIMPLEX, 1, text_color, 2, cv2.LINE_AA)
    return out_image


# Function to run detection on video and save annotated video
def detection(source, name):

    video_cap = cv2.VideoCapture(source)

    fps = video_cap.get(cv2.CAP_PROP_FPS)
    frame_width = video_cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    frame_height = video_cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    resolution = (int(frame_width), int(frame_height))

    print(f"Resolution: {int(frame_width)}x{int(frame_height)}")

    video_frames = []

    x = 1
    while video_cap.isOpened():
        ret, frame = video_cap.read()
        if not ret:
            break

        # Get Channel Information
        if x == 1:
            channels = frame.shape[2]
            x += 1

        cls, conf, box = yolov10_detection(model, frame)
        detection_output = list(zip(cls, conf, box))
        image_output = draw_box(frame, detection_output, label_map, COLORS)

        image_output = draw_fps(fps, image_output)
        video_frames.append(image_output)

    file_name = get_name(source)
    folder_name = name
    save_path = get_save_path(file_name, folder_name)
    out = cv2.VideoWriter(save_path, cv2.VideoWriter_fourcc(*'mp4v'), int(fps), resolution)
    
    for frame in video_frames:
        out.write(frame)
    out.release()
    video_cap.release()

    print(f"fps : {fps}")
    print(f"channels : {channels}")
    return resolution, channels

main_folder = 'Alarms'
subfolders = ['car_park', 'person_only', 'person_vehicle_movement', 'special_case', 'vehicle_only']

# Initialize variables for storing profiling results
num = 1
index = []
time_diff = []
cpu_usages = []
memory_usages = []
gpu_usages = []
resolutions = []
channels_list = []

Total_pipeline_time = 0

# Process each video file in the subfolders
for subfolder in subfolders:
    folder_path = os.path.join(main_folder, subfolder)
    for file_name in os.listdir(folder_path):
        if file_name.endswith('.mp4'):
            source = os.path.join(folder_path, file_name)
            print(f"Video file : {source}")
            
            start_t = time.time()
            resolution, channels = detection(source, f"detection-yolov10/{subfolder}")
            end_t = time.time()

            pipeline_time = end_t - start_t

            Total_pipeline_time+=pipeline_time

            # Get system usage metrics
            cpu_usage = psutil.cpu_percent()
            process_memory = psutil.Process().memory_info().rss / 1024 ** 2
            process_gpu_usage = torch.cuda.max_memory_allocated() / 1024 ** 2
            
            # Append metrics to respective lists
            index.append(num)
            num += 1
            time_diff.append(pipeline_time)
            cpu_usages.append(cpu_usage)
            memory_usages.append(process_memory)
            gpu_usages.append(process_gpu_usage)
            resolutions.append(f"{resolution[0]}x{resolution[1]}")
            channels_list.append(channels)

            # Print profiling results for the current video
            print('Pipeline time: ' + str(pipeline_time) + ' seconds' + '\n')
            print('cpu_usage: ' + str(cpu_usage) + ' %' + '\n')
            print('Pipeline memory: ' + '%.2f' % process_memory + ' MB' + '\n')
            print('Pipeline GPU usage: ' + '%.2f' % process_gpu_usage + ' MB' + '\n')

print('Total Pipeline Time: ' + str(Total_pipeline_time) + ' seconds' + '\n')
# Compile profiling results into a DataFrame
data = {
    'Video Index': index,
    'Time Difference (s)': time_diff,
    'CPU Usage (%)': cpu_usages,
    'Memory Usage (MB)': memory_usages,
    'GPU Usage (MB)': gpu_usages,
    'Resolution': resolutions,
    'Channels': channels_list
}

df = pd.DataFrame(data)

# Save profiling results to an Excel file
excel_file = 'detection_profiling_results_yolov10x.xlsx'
df.to_excel(excel_file, index=False)
print(f"Profiling results saved to {excel_file}")

import matplotlib.pyplot as plt

# Plot the profiling data
plt.figure(figsize=(14, 8))

# Plot Time Taken for detection
plt.subplot(4, 1, 1)
plt.plot(index, time_diff, label='Time Taken(S) ')
plt.xlabel('Video')
plt.ylabel(' Detection Time per Video')
plt.title('Detection Time Per Video ')
plt.legend()

# Plot CPU usage over time
plt.subplot(4, 1, 2)
plt.plot(index, cpu_usages, label='CPU Usage (%)')
plt.axhline(y=20, color='r', linestyle='--', label='Threshold (20%)')
plt.xlabel('Video')
plt.ylabel('CPU Usage (%)')
plt.title('CPU Usage Over Time')
plt.legend()

# Plot memory usage over time
plt.subplot(4, 1, 3)
plt.plot(index, memory_usages, label='Memory Usage (MB)')
plt.axhline(y=4096, color='r', linestyle='--', label='Threshold (4096 MB)')
plt.xlabel('Video')
plt.ylabel('Memory Usage (MB)')
plt.title('Memory Usage Over Time')
plt.legend()

# Plot GPU usage over time
plt.subplot(4, 1, 4)
plt.plot(index, gpu_usages, label='GPU Usage (MB)')
plt.axhline(y=8192, color='r', linestyle='--', label='Threshold (8192 MB)')
plt.xlabel('Video')
plt.ylabel('GPU Usage (MB)')
plt.title('GPU Usage Over Time')
plt.legend()

# Adjust layout and display the plots
plt.tight_layout()
plt.show()