# Real-time Object Detection with YOLOv10

## Introduction

This project implements real-time object detection using the YOLOv10 model. It processes video frames, generates performance metrics, and produces annotated videos and graphs. The project aims to achieve efficient object detection leveraging multiprocessing techniques for constrained devices.

## Pipeline
![Object Detection Example](images/pipeline.png)

## Prerequisites

- Python 3.9
- Conda (for managing dependencies)
- CUDA-compatible GPU
- CUDA Toolkit installed based on your GPU specifications

## Installation

### Setup Conda Virtual Environment

1. Create and activate a Conda virtual environment:

    ```bash
    conda create -n <env_name> python=3.9
    conda activate <env-name>
    ```

### Install CUDA

2. Install CUDA Toolkit from NVIDIA's official website based on your GPU:

    - [NVIDIA CUDA Toolkit](https://developer.nvidia.com/cuda-downloads)

### Clone the Repository

3. Clone the YOLOv10 repository:

    ```bash
    git clone https://github.com/THU-MIG/yolov10.git
    cd yolov10
     ```

    Note:

    Before executing the following command make comment on torch==2.0.1,
    torchvision==0.15.2 libraries inside the yolov10/requirement.txt file

    ```bash
    pip install .
    cd ..

    ```

### Install Required Packages

4. Install dependencies:


    ```bash
    pip install -r requirements.txt
    ```

### Video Folder Structure


## Alarms

- **vehicle_only**: Contains videos related to vehicle-only alarms.
- **special_case**: Contains videos related to special case alarms.
- **person_vehicle_movement**: Contains videos related to person and vehicle movement alarms.
- **person_only**: Contains videos related to person-only alarms.
- **car_park**: Contains videos related to car park alarms.

Each of these subfolders contains video files pertinent to their respective categories.



## Script Execution

### 1. Object Detection Script

- Execute the following command to object detection on videos which is inside the same directory:

    ```bash
    python Object Detection.py
    ```

## Results for script1

- Upon running the script, it captures and logs several performance metrics crucial for evaluating the object detection process in real-time. These metrics include the time taken to process each video frame, CPU utilization percentage, GPU usage statistics, and memory consumption. The recorded data is then compiled into an Excel file located in the project's working directory. Additionally, the script automatically generates graphical representations of these metrics, offering visual insights into the system's performance over the duration of the video processing. Furthermore, the script enhances the video output by annotating detected objects, and these annotated videos are saved in a directory specified within the script. This comprehensive approach ensures both quantitative data collection and qualitative visual feedback, facilitating thorough analysis and optimization of the object detection pipeline.


### 2. Multiprocessing

- Utilize multiprocessing for enhanced performance:

    ```bash
    python MultiProcessing.py
    ```

## Results for script2

![Object Detection Example](images/Constraints_b.png)

- The function processes four videos in parallel until completion, adhering to hardware constraints: 8GB of GPU VRAM, 4GB of memory, and a maximum CPU usage of 20%. Upon completion, it records and stores performance metrics including CPU usage (%), GPU usage, and memory usage in a CSV file located in the project's working directory. Additionally, the script generates performance graphs to visualize these metrics, allowing evaluation against the specified device constraints. Annotated videos, featuring detected objects, are saved in their respective directories as configured within the script, providing enhanced visual insights alongside quantitative performance data.



<!-- ## Contribution

- Contributions are welcome via pull requests.
- Fork the repository, make improvements, and enhance functionality.

## License

- This project is licensed under the [License Name] License.
- See the [LICENSE](LICENSE) file for details. -->
